# README #

This is a tutorial project for the PIT-Hackathon in September 2018.

### What is shown ###

* Usage of behavior trees for ai design
* Usage of post production effects


### Points of Interest ###

Take a look at the following scripts:

* 012_Pong / Assets / _nvp / scripts / nvp_BallManager_scr.cs





### Who do I talk to? ###

* b.rubow (at) t-online.de
