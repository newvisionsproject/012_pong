﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using newvisionsproject.managers.events;
using newvisionsproject.factories;
using NvpFluentBehaviorTree;
using NvpFluentBehaviorTree.Nodes;

namespace newvisionsproject.pong.ball
{
    public enum BallStates{
        Instantiated,
        Moving,
        outOfBounds
    }


    public class nvp_BallManager_scr : MonoBehaviour
    {

        // +++ editor fields ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        public float speed;                                         // the speed of the ball                               




        // +++ private fields +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        private Transform _transform;                               // cache of the gameobjects transform component to speed up (urban legend???)
        public Vector3 _direction;                                  // the direction the ball moves in
        private BounceFromPlayerCalculator _bounceCalculator;       // a class that calculates how the ball bounces of the player
        IBehaviorTreeNode _behaviorTree;                            // storage for the ball ai
        TimeData _time;                                             // TODO: replace by float
        private float _speedCached;                                 // cache for the editor speed value

        // +++ sensor values +++
        private Vector3 _lastPosition;                              // cache for the ball position in the last frame
        private bool _hasHitWall;                                   // sensor indicator that the ball has hit a wall
        private bool _hasHitPlayer;                                 // sensor indicator that the ball has hit a player
        private bool _isOutOfBounds;                                // sensor indicator that the ball left the play area
        private bool _isStarting;                                   // sensor indicator that the scene was started for the first time




        // +++ unity callbacks ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        void Start()
        {
            // set some pre game values
            InitBall();

            // build the ball ai
            BuildBehaviorTree();
        }

        void Update()
        {
            // TODO: replace with float
            _time.deltaTime = Time.deltaTime;

            // evaluate the behavior tree
            _behaviorTree.Tick(_time);

            // cache calculated postion for jumping to a position
            // before the ball was hitting something
            _lastPosition = _transform.position;
        }




        // +++ event handler ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        void OnTriggerEnter(Collider other)
        {
            /**
             * Is called, if the ball collider enters another collider which is set up as trigger.
             * 
             * params:
             * other (collider): the collider component of the gameobject this collider just entered.
             **/


            if (other.tag == "wall"){ 
                nvp_EventManager_scr.INSTANCE
                    .InvokeEvent(
                        GameEvents.onBallHitsWall, 
                        this, 
                        other.gameObject
                    );
                _hasHitWall = true;
            }
            
            else if (other.tag == "player"){
                nvp_EventManager_scr.INSTANCE
                    .InvokeEvent(
                        GameEvents.onBallHitsPlayer,
                         this, 
                         other.gameObject
                    );
                _hasHitPlayer = true;                
            }
        }



        // +++ coroutines +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        IEnumerator ControlBallSpeed(){
            /**
             * used to control the ball speed within the game. Takes care
             * that the ball gets faster over time so the game will be lost
             **/

            speed = 0f;
            yield return new WaitForSeconds(2.0f);
            speed = _speedCached;
            while(true){
                yield return new WaitForSeconds(0.5f);
                speed += 0.025f;
            }
        }




        // +++ functions ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        
        private void BuildBehaviorTree()
        {
            /**
             * Build the behavior of the ball. This behavior can certain nodes that either can
             * do something or can be used as containers for other nodes.
             * 
             * possible nodes:
             * ---------------
             * 
             * - parallel node (container node) : call all nodes in the container
             * - sequence node (container node) : call all nodes after one onother (stop sequence, if on node is failing)
             * - acition node                   : do something that can succeed or fail
             **/
            var btb = new BehaviourTreeBuilder();
            _behaviorTree = btb
                .Parallel("main behavior", int.MaxValue, int.MaxValue)
                    .Sequence("start ball")
                        .Do("is out of bounds", t =>
                        {
                            if (_isOutOfBounds || _isStarting)
                            {
                                _isOutOfBounds = false;
                                _isStarting = false;
                                return BehaviorTreeStatus.Success;
                            }
                            return BehaviorTreeStatus.Failure;
                        })                        
                        .Do("reset ball position", t =>
                        {
                            _transform.position = Vector3.zero;
                            return BehaviorTreeStatus.Success;
                        })
                        .Do("set random direction", t =>
                        {
                            _direction = this.GetRandomDirection();
                            return BehaviorTreeStatus.Success;
                        })
                        .Do("Start Ball Speed controller", t =>
                        {
                            StopCoroutine("ControlBallSpeed");
                            StartCoroutine(ControlBallSpeed());
                            return BehaviorTreeStatus.Failure;
                        })
                    .End()
                    .Sequence("react to wall hit")
                        .Do("sense wall hit", t =>
                        {
                            if (this._hasHitWall)
                            {
                                return BehaviorTreeStatus.Success;
                            }
                            return BehaviorTreeStatus.Failure;
                        })
                        .Do("change vertical direction", t =>
                        {
                            this._hasHitWall = false;
                            _direction.y *= -1.0f;
                            _transform.position = _lastPosition;
                            return BehaviorTreeStatus.Running;
                        })
                    .End()
                    .Sequence("")
                        .Do("sense player hit", t =>
                        {
                            if (_hasHitPlayer) return BehaviorTreeStatus.Success;
                            return BehaviorTreeStatus.Failure;
                        })
                        .Do("change vertical direction", t =>
                        {
                            _hasHitPlayer = false;
                            _direction = _bounceCalculator.CalcBounceDirection(_direction);
                            _transform.position = _lastPosition;
                            return BehaviorTreeStatus.Running;
                        })
                    .End()
                    .Do("move ball", t =>
                    {
                        _transform.Translate(_direction * t.deltaTime * speed, Space.World);
                        return BehaviorTreeStatus.Success;
                    })
                    .Sequence("check for player scores")
                        .Do("check for out of bounds", t =>
                        {
                            if (Mathf.Abs(_transform.position.x) > 20.0f)
                            {
                                speed = 0.0f;
                                _isOutOfBounds = true;
                                return BehaviorTreeStatus.Success;
                            }
                            else{
                                return BehaviorTreeStatus.Failure;
                            }
                        })
                        .Do("invoke out of bounds event", t => {
                                nvp_EventManager_scr.INSTANCE.InvokeEvent(
                                    GameEvents.onBallOutOfBounds, 
                                    this, 
                                    this.transform.position.x);
                                return BehaviorTreeStatus.Success;

                        })
                    .End()
                .End()
                .Build();
        }

        private void InitBall()
        {
            /*
             * Just set some values at the start of the game
             **/

            // cache transform
            _transform = this.transform;

            // cache the setted speed to resolve it later in the game
            _speedCached = speed;

            // TODO: replace by float
            _time = new TimeData();

            _bounceCalculator = BounceCalcutorFactory.GetCalculator(BounceFromPlayerCalculationMethods.Random);
            
            // when scene is show first time, flag that the game is starting for the first time
            _isStarting = true;
        }

        private Vector3 GetRandomDirection()
        {
            /**
             * Calculate a random start vector for the ball
             */

            // get -1 or +1            
            var sign = this.RandomSignedOne();

            // calc random normalized vector 3
            Vector3 dir = new Vector3(
                (Random.value + 0.5f) * sign,
                Random.value * 0.5f,
                0
            ).normalized;
            return dir;
        }

        private float RandomSignedOne (){
            var sign = Mathf.Sign(0.5f -Random.value);
            return sign;
        }
    }

    
}


