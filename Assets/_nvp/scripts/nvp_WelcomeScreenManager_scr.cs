﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class nvp_WelcomeScreenManager_scr : MonoBehaviour {

	// +++ fields +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	public string gameSceneName;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyUp(KeyCode.Alpha1)) {
			SceneManager.LoadScene(gameSceneName);
		}
		else if (Input.GetKeyUp(KeyCode.Alpha2)){
			SceneManager.LoadScene(gameSceneName);
		}
	}
}
