﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using newvisionsproject.managers.events;

namespace newvisionsproject.pong
{
    public class nvp_UiManager : MonoBehaviour
    {

        // +++ editor fields ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        [SerializeField] TextMeshProUGUI player1Score;
        [SerializeField] TextMeshProUGUI player2Score;




        // +++ fields +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        public nvp_GameManager_scr _gameManager;




        // +++ unity callbacks ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        void Start()
        {
            nvp_EventManager_scr.INSTANCE.SubscribeToEvent(GameEvents.onPlayerScored, this.onPlayerScored);
        }




        // +++ event handler ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        void onPlayerScored(object sender, object eventArgs)
        {
            // Debug.Log("nvp_UiManager: onPlayerScored");
            var scoreData = (ScoreData)eventArgs;

            switch(scoreData.scoringPlayer){
                case 1:
                    player1Score.text = scoreData.playerScore.ToString("00");
                    break;

                case 2:
                    player2Score.text = scoreData.playerScore.ToString("00");
                    break;
            }
        }
    }

    public struct ScoreData{
        public int scoringPlayer;
        public int playerScore;
    }
}
