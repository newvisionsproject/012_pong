﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using newvisionsproject.managers.events;
using NvpFluentBehaviorTree;
using NvpFluentBehaviorTree.Nodes;
using newvisionsproject.pong.ball;



namespace newvisionsproject.pong.player
{
    public class nvp_PlayerManager_scr : MonoBehaviour
    {

        // +++ inspector fields +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        public float velocity;                              // used by pyhsics calculations
        public float drag;                                  // used by physics calculations
        public nvp_PIDController PIDController;
        public float maxMoveDistance;                       




        // +++ fields +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        IBehaviorTreeNode _playerBehaviorTree;              // holds the behavior tree for the current gameobject
        Transform _ball;                                    // a reference to the balls position, for calculation ai movement
        nvp_BallManager_scr _ballScript;                    // a reference to the ball script, which contains the direction of the ball
        Transform _transform;                               // a cache to the gameobjects transform to optimize performance (urban legend)
        Vector3 _currentVelocity;                           // the current physical velocity of the player
        TimeData _time;                                     // a time-struct to pass to the behavior tree
        public float _aiInput;                              // stores the calculated input for the ai




        // +++ unity callbacks ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        void Start()
        {
            Init();

            // TODO: hardcoded 1 player game, change this to be set from welcome screen
            if (this.gameObject.name == "player1") _playerBehaviorTree = BehaviorTreeFactory("human");
            if (this.gameObject.name == "player2") _playerBehaviorTree = BehaviorTreeFactory("aiSimple");
        }

        void Update()
        {
            _time.deltaTime = Time.deltaTime;
            _playerBehaviorTree.Tick(_time);
            _transform.position += _currentVelocity;            
        }




        // +++ functions ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        private void Init()
        {
            _currentVelocity = Vector3.zero;

            _time = new TimeData();

            // Collect References
            _transform = transform;
            _ball = GameObject.Find("ball").transform;
            _ballScript = _ball.GetComponent<nvp_BallManager_scr>();
        }






        // +++ behavior factory +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        private IBehaviorTreeNode BehaviorTreeFactory(string requestBehaviorToBuild){
            switch(requestBehaviorToBuild){
                case "human":
                    return GetHumanBehavior();

                case "aiSimple":
                    return GetAiSimpleBehavior();       
                
                default:
                    return GetHumanBehavior();
            }
        }

        private IBehaviorTreeNode GetHumanBehavior(){
            var behaviorBuilder = new NvpFluentBehaviorTree.BehaviourTreeBuilder();

            var behaviorTree = behaviorBuilder
                    .Parallel("root behavior", int.MaxValue, int.MaxValue)
                        .Sequence("move player by physics")
                            .Do("add velocity", t => {

                                _currentVelocity += Vector3.up * Input.GetAxis("Vertical") * velocity * Mathf.Pow(t.deltaTime, 2);

                                return BehaviorTreeStatus.Success;
                            })
                            .Do("add drag", t => {
                                _currentVelocity *= (1-drag);
                                return BehaviorTreeStatus.Success;
                            })
                        .End()
                        .Sequence("check for out of bounds")
                            .Do("is max range left", t => {
                                if(Mathf.Abs(_transform.position.y) < maxMoveDistance) return BehaviorTreeStatus.Failure;
                                return BehaviorTreeStatus.Success;
                            })
                            .Do("invert velocity", t => {
                                _currentVelocity.y *= -1f;
                                return BehaviorTreeStatus.Success;
                            })
                            .Do("reset the player position", t => {
                                _transform.position = new Vector3(
                                    _transform.position.x,
                                    maxMoveDistance * Mathf.Sign(_transform.position.y),
                                    0
                                );
                                return BehaviorTreeStatus.Success;
                            })
                            .Do("call hit wall event", t => {
                                nvp_EventManager_scr.INSTANCE.InvokeEvent(GameEvents.onPlayerHitsWall, this, null);
                                return BehaviorTreeStatus.Success;
                            })
                        .End()
                    .End()
                .Build();

            return behaviorTree;
        }

        private IBehaviorTreeNode GetAiSimpleBehavior(){
            var behaviorBuilder = new NvpFluentBehaviorTree.BehaviourTreeBuilder();

            var behaviorTree = behaviorBuilder
                    .Parallel("root behavior", int.MaxValue, int.MaxValue)
                        
                        // +++++++++++++++++++
                        // +++ Follow Ball +++
                        // +++++++++++++++++++
                        .Sequence("Follow Ball")
                            .Do("is ball approching", t => {
                                if(Mathf.Sign(_transform.position.x) == Mathf.Sign(_ballScript._direction.x)) return BehaviorTreeStatus.Success;
                                return BehaviorTreeStatus.Failure;
                            })
                            .Do("calculate ai input by PID Controller", t => {
                                float error = _ball.transform.position.y - _transform.position.y;
                                _aiInput = PIDController.Update(error);
                                _aiInput = Mathf.Clamp(_aiInput, -1f, 1f);
                                return BehaviorTreeStatus.Success;
                            })
                            .Do("add velocity", t => {

                                _currentVelocity += Vector3.up * _aiInput * velocity * Mathf.Pow(t.deltaTime, 2);

                                return BehaviorTreeStatus.Success;
                            })
                            .Do("add drag", t => {
                                _currentVelocity *= (1-drag);
                                return BehaviorTreeStatus.Success;
                            })
                        .End()

                        // +++++++++++++++++++++
                        // +++ Center player +++
                        // +++++++++++++++++++++
                        .Sequence("return to center")
                            .Do("is ball moving away", t => {
                                if(Mathf.Sign(_transform.position.x) != Mathf.Sign(_ballScript._direction.x)) return BehaviorTreeStatus.Success;
                                return BehaviorTreeStatus.Failure;
                            })
                            .Do("calculate ai input by PID Controller", t => {    
                                // Debug.Log("move center");        
                                float error = -_transform.position.y;
                                _aiInput = PIDController.Update(error);
                                _aiInput = Mathf.Clamp(_aiInput, -1f, 1f);
                                return BehaviorTreeStatus.Success;
                            })
                            .Do("add velocity", t => {

                                _currentVelocity += Vector3.up * _aiInput * velocity * Mathf.Pow(t.deltaTime, 2);

                                return BehaviorTreeStatus.Success;
                            })
                            .Do("add drag", t => {
                                _currentVelocity *= (1-drag);
                                return BehaviorTreeStatus.Success;
                            })
                        .End()

                        // +++++++++++++++++++++++++++++
                        // +++ stay inside playfield +++
                        // +++++++++++++++++++++++++++++
                        .Sequence("check for out of bounds")
                            .Do("is max range left", t => {
                                if(Mathf.Abs(_transform.position.y) < maxMoveDistance) return BehaviorTreeStatus.Failure;
                                return BehaviorTreeStatus.Success;
                            })
                            .Do("invert velocity", t => {
                                _currentVelocity.y *= -1f;
                                return BehaviorTreeStatus.Success;
                            })
                            .Do("reset the player position", t => {
                                _transform.position = new Vector3(
                                    _transform.position.x,
                                    maxMoveDistance * Mathf.Sign(_transform.position.y),
                                    0
                                );
                                return BehaviorTreeStatus.Success;
                            })
                            .Do("call hit wall event", t => {
                                nvp_EventManager_scr.INSTANCE.InvokeEvent(GameEvents.onPlayerHitsWall, this, null);
                                return BehaviorTreeStatus.Success;
                            })
                        .End()

                    .End()
                .Build();

            return behaviorTree;
        }
    }
}


