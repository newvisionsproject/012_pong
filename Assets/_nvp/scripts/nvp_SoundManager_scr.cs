﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using newvisionsproject.managers.events;

public class nvp_SoundManager_scr : MonoBehaviour {

	// +++ fields +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	public AudioSource boingSound;
	public AudioSource releaseSound;

	void Start () {
		nvp_EventManager_scr.INSTANCE.SubscribeToEvent(GameEvents.onBallHitsPlayer, this.onBallHitsPlayer);
		nvp_EventManager_scr.INSTANCE.SubscribeToEvent(GameEvents.onBallHitsWall, this.onBallHitsWall);
		nvp_EventManager_scr.INSTANCE.SubscribeToEvent(GameEvents.onPlayerHitsWall, this.onPlayerHitsWall);
		nvp_EventManager_scr.INSTANCE.SubscribeToEvent(GameEvents.onBallReleased, this.onBallReleased);
	}


	// +++ event handler ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	void onBallHitsPlayer(object sender, object eventArgs){
		this.boingSound.pitch = 1;
		this.boingSound.Play();
	}


	void onBallHitsWall(object sender, object eventArgs){
		this.boingSound.pitch = 2;
		this.boingSound.Play();
	}

	void onPlayerHitsWall(object sender, object eventArgs){
		this.boingSound.pitch = 0.5f;
		this.boingSound.Play();
	}

	void onBallReleased(object sender, object eventArgs){
		this.releaseSound.Play();
	}
}
